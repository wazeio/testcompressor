'use strict';

const assert = require('assert');
const compressor = require('../compressor');

describe('Test compressor module', () => {
    it('[1,2,3,4,5,6,7,8] -> "1-8"', done => {
        let list = [1,2,3,4,5,6,7,8];
        compressor(list, (err, result) => {
            assert.equal(err, null);
            assert.equal(result, '1-8');
            done();
        });
    });
    it('[1,3,4,5,6,7,8] -> "1,3-8"', done => {
        let list = [1,3,4,5,6,7,8];
        compressor(list, (err, result) => {
            assert.equal(err, null);
            assert.equal(result, '1,3-8');
            done();
        });
    });
    it('[1,3,4,5,6,7,8,10,11,12] -> "1,3-8,10-12"', done => {
        let list = [1,3,4,5,6,7,8,10,11,12];
        compressor(list, (err, result) => {
            assert.equal(err, null);
            assert.equal(result, '1,3-8,10-12');
            done();
        });
    });
    it('[1,2,3] -> "1-3"', done => {
        let list = [1,2,3];
        compressor(list, (err, result) => {
            assert.equal(err, null);
            assert.equal(result, '1-3');
            done();
        });
    });
    it('[1,2] -> "1,2"', done => {
        let list = [1,2];
        compressor(list, (err, result) => {
            assert.equal(err, null);
            assert.equal(result, '1,2');
            done();
        });
    });
    it('[1,2,4] -> "1,2,4"', done => {
        let list = [1,2,4];
        compressor(list, (err, result) => {
            assert.equal(err, null);
            assert.equal(result, '1,2,4');
            done();
        });
    });
    it('[1,2,4,5,6] -> "1,2,4-6"', done => {
        let list = [1,2,4,5,6];
        compressor(list, (err, result) => {
            assert.equal(err, null);
            assert.equal(result, '1,2,4-6');
            done();
        });
    });
    it('[1,2,3,7,8,9,15,17,19,20,21] -> "1-3,7-9,15,17,19-21"', done => {
        let list = [1,2,3,7,8,9,15,17,19,20,21];
        compressor(list, (err, result) => {
            assert.equal(err, null);
            assert.equal(result, '1-3,7-9,15,17,19-21');
            done();
        });
    });
    it('[1,2,3,4,5,6,100,1091,1999,2000,2001,2002] -> "1-6,100,1091,1999-2002"', done => {
        let list = [1,2,3,4,5,6,100,1091,1999,2000,2001,2002];
        compressor(list, (err, result) => {
            assert.equal(err, null);
            assert.equal(result, '1-6,100,1091,1999-2002');
            done();
        });
    });
    it('[1] -> "1"', done => {
        let list = [1];
        compressor(list, (err, result) => {
            assert.equal(err, null);
            assert.equal(result, '1');
            done();
        });
    });
    it('[1,3,5,7,9,11] -> "1,3,5,7,9,11"', done => {
        let list = [1,3,5,7,9,11];
        compressor(list, (err, result) => {
            assert.equal(err, null);
            assert.equal(result, '1,3,5,7,9,11');
            done();
        });
    });
});