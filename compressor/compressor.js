function compress(list) {
    'use strict';
    var result = [list[0]];
    for (let i=1; i<list.length; i++) {
        let x1 = list[i];
        let x0 = list[i - 1];
        let dx = x1 - x0;
        if (dx > 1 || isNaN(dx)) {
            let index = result.length - 1;
            let initial = result[index];
            let di = x0 - initial;
            if (di === 1) {
                result.push(x0);
            } else if (di > 1) {
                result[index] += '-' + x0;
            }
            result.push(x1);
        }
        else if (i === list.length - 1) {
            let index = result.length - 1;
            let initial = result[index];
            let di = x1 - initial;
            if (di > 1) {
                result[index] += '-' + x1;
            } else {
                result.push(x1);
            }
            break;
        }
    }
    return result.toString();
}
process.on('message', function(list) {
    process.send(compress(list));
    process.exit();
});