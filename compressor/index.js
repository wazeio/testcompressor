'use strict';
const childProcess = require('child_process');

module.exports = function(list, callback) {
    /*
    * Для асинхронности было бы достаточно использовать process.nexTick
    * Однако nextTick просто переносит исполнение следом за скопом по event loop
    * Что является псевдоасинхронным, поэтому было использовано выделение функции в отдельный процесс
    * P.S. в случае маленьких массивов это не рационально. Для запуска нового процесса требуется время
    * P.P.S. Выделять всего 1 процесс на несколько вычислений не рационально для ресурсоемких задач,
    * поэтому на каждый запуск компрессора создается новый дочерний процесс
    * */
    var compressor = childProcess.fork(__dirname + '/compressor.js');
    compressor.on('message', result => {
        callback(null, result);
    });
    compressor.on('error', callback);
    compressor.send(list);
};